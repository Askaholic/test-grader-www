-- Your SQL goes here
CREATE TABLE assignment(
  id SERIAL PRIMARY KEY,
  name varchar(255),
  language varchar(255),
  testFile varchar(255)
);
