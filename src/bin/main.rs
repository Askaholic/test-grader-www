#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate rocket;

extern crate rocket_contrib;
extern crate tera;

extern crate test_grader;
extern crate test_grader_www;

use std::path::Path;

use rocket_contrib::templates::Template;
use tera::Context;

use test_grader_www::request_guards::MultipartData;


#[get("/")]
fn index() -> Template {
    let config = test_grader_www::config::get_config()
        .expect("Config file not available");

    let mut assignments: Vec<String> = config.assignments.into_iter()
        .map(|(key, _value)| key)
        .collect();
    assignments.sort();

    let mut context = Context::new();
    context.add("title", &"Test Grader");
    context.add("assignments", &assignments);
    Template::render("base", &context)
}

#[get("/<assignment>")]
fn assignment(assignment: String) -> Option<Template> {
    let config = test_grader_www::config::get_config()
        .expect("Config file not available");
    // 404 if the requested assignment does not exist
    config.assignments.get(&assignment)?;

    let mut context = Context::new();
    context.add("assignment", &assignment);
    Some(Template::render("upload", &context))
}

#[post("/<assignment>/upload", format = "multipart/form-data", data = "<data>")]
fn upload_assignment(assignment: String, mut data: MultipartData) -> std::io::Result<Option<Template>> {
    let config = test_grader_www::config::get_config()
        .expect("Config file not available");
    let tmp_dir = config.environment.temp_directory;
    let config = match config.assignments.get(&assignment) {
        Some(c) => c,
        None => return Ok(None)
    };

    let entries = data.save()?;
    let multipart_dir = entries.save_dir.into_path();

    let file_path = multipart_dir.read_dir().expect("Failed to read multipart dir")
        .next().ok_or(std::io::Error::new(std::io::ErrorKind::NotFound, "Missing uploaded data"))??
        .path();

    let assignment = test_grader::Assignment::new(
        config.container_name.clone(),
        Path::new(&config.test_program).to_path_buf(),
        Path::new(&tmp_dir).to_path_buf()
    ).rename_test_file(Path::new(&config.rename_test_file).to_path_buf());

    let output = assignment.grade(file_path);

    let mut context = Context::new();
    match output {
        Err(e) => context.add("error", &e.to_string()),
        Ok(o) => {
            context.add("output", &o.output);
            context.add("score", &format!("{}/{}", o.passed, o.tests));
        }
    }
    Ok(Some(Template::render("report", &context)))
}

fn main() {
    rocket::ignite()
           .mount("/", routes![
                index,
                assignment,
                upload_assignment
            ])
           .attach(Template::fairing())
           .launch();
   }
