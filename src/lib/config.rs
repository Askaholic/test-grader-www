use serde_derive::Deserialize;
use std::collections::HashMap;

use std::fs::File;
use std::io::Read;

use std::io;


static CONFIG_FILE_PATH: &'static str = "TestGrader.toml";


#[derive(Deserialize, Debug)]
pub struct Config {
    pub environment: EnvironmentConfig,
    pub assignments: HashMap<String, AssignmentConfig>
}

#[derive(Deserialize, Debug)]
pub struct EnvironmentConfig {
    pub temp_directory: String
}

#[derive(Deserialize, Debug)]
pub struct AssignmentConfig {
    pub container_name: String,
    pub test_program: String,
    pub rename_test_file: String
}

pub fn get_config() -> io::Result<Config> {
    let mut f = File::open(CONFIG_FILE_PATH)?;
    let mut contents = String::new();
    f.read_to_string(&mut contents)?;

    toml::from_str(&contents)
        .map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e.to_string()))
}
