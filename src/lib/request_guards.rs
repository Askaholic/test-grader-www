use multipart::server::{Multipart, Entries};

use rocket::{Data, Outcome};
use rocket::http::Status;
use rocket::request::{Request};
use rocket::data::{self, FromDataSimple, DataStream};

pub struct MultipartData(Multipart<DataStream>);
impl FromDataSimple for MultipartData {
    type Error = ();

    fn from_data(req: &Request, data: Data) -> data::Outcome<Self, Self::Error> {
        let content_type = match req.content_type() {
            Some(ct) => ct,
            None => return Outcome::Failure((Status::BadRequest, ()))
        };

        if !content_type.is_form_data() {
            return Outcome::Failure((Status::BadRequest, ()));
        }

        let boundary = match content_type.params().find(|&(k, _)| k == "boundary") {
            Some((_, boundary)) => boundary,
            None => return Outcome::Failure((Status::BadRequest, ()))
        };

        Outcome::Success(MultipartData(Multipart::with_body(data.open(), boundary)))
    }
}

impl MultipartData {
    pub fn save(&mut self) -> std::io::Result<Entries> {
        use multipart::server::save::SaveResult::*;

        match self.0.save()
            .size_limit(1048576)
            .memory_threshold(0)
            .temp() {
            Full(entries) => Ok(entries),
            Partial(_, reason) => Err(
                std::io::Error::new(
                    std::io::ErrorKind::InvalidData, format!("{:?}", reason)
                )
            ),
            Error(e) => Err(e),
        }
    }
}
